package org.example;

public class TablePrinter {

    private static void printSeparator(int columns, int columnWidth) {
        for (int i = 0; i < columns; i++) {
            System.out.print("+");
            for (int j = 0; j < columnWidth - 1; j++) {
                System.out.print("-");
            }
        }
        System.out.println("+");
    }

    private static void printData(String[] words, int columnWidth) {
        int offset = 2;
        for (String word : words) {
            System.out.print("|");
            for (int j = 0; j < columnWidth - 1; j++) {
                int wordIndex = j - offset;
                char ch = (wordIndex > word.length() - 1 || j < offset) ? ' ' : word.charAt(wordIndex);
                System.out.print(ch);
            }
        }
        System.out.println("|");
    }

    public static void printSingleRow(String[] data, int columnWidth) {
        printSeparator(data.length, columnWidth);
        printData(data, columnWidth);
    }

    public static void printTable(String[][] data, int columnWidth) {
        for (int i = 0; i < data.length; i++) {
            String[] row = data[i];
            printSeparator(row.length, columnWidth);
            printData(row, columnWidth);
            if (i != data.length - 1) continue;
            printSeparator(row.length, columnWidth);
        }

    }

    public static void main(String[] args) {
        TablePrinter.printTable(new String[][]{
                new String[]{"a", "b", "c"},
                new String[]{"a", "b", "c"},
                new String[]{"a", "b", "c"},
                new String[]{"a", "b", "c"},
        }, 20);
    }
}
