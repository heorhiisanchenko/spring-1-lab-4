package org.example;

import java.util.List;
import java.util.stream.Stream;

public class TableBuilder<T> {

    public static String[] createHeader() {
        return new String[]{"Field", "Type", "Null", "Key", "Default", "Extra"};
    }
    public String[][] createTable(T instance) {
        List<String[]> rowList = Stream.of(instance.getClass().getDeclaredFields()).map(field -> {
            Column column = field.getAnnotation(Column.class);
            return new String[]{
                    column.name(),
                    column.type(),
                    column.notNull() ? "NO" : "YES",
                    column.primary_key() ? "PRI" : "",
                    column.defaultVal(),
                    column.auto_increment() ? "auto_increment" : ""
            };
        }).toList();
        return rowList.toArray(new String[0][0]);
    }
}
