package org.example;

import java.util.Date;

@Entity(tableName = "user")
public class User {
    @Column(name = "id", type = "INT", primary_key = true, auto_increment = true, notNull = true)
    public int id;

    @Column(name = "username", type = "VARCHAR(200)")
    public String username;

    @Column(name = "password", type = "VARCHAR(200)")
    public String password;

    @Column(name = "birthday", type = "DATE", defaultVal = "1997.12.05")
    public Date birthday;
}
