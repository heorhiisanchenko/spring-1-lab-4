package org.example;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
    String name();

    String type();

    boolean auto_increment() default false;

    boolean primary_key() default false;

    boolean notNull() default false;

    String defaultVal() default "NULL";
}
