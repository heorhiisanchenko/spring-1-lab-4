import org.example.Entity;
import org.example.TableBuilder;
import org.example.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserTest {

   @Test
   public void shouldContainClassAnnotation() {
      User user = new User();
      String expected = "user";
      String actual = user.getClass().getAnnotation(Entity.class).tableName();
      Assertions.assertEquals(expected, actual);
   }

   @Test
   public void shouldContainFieldAnnotations() {
      User user = new User();
      String[][] expected = new String[][]{
              new String[]{"id", "INT", "NO", "PRI", "NULL", "auto_increment"},
              new String[]{"username", "VARCHAR(200)", "YES", "", "NULL", ""},
              new String[]{"password", "VARCHAR(200)", "YES", "", "NULL", ""},
              new String[]{"birthday", "DATE", "YES", "", "1997.12.05", ""},
      };
      String[][] actual = new TableBuilder<User>().createTable(user);
      Assertions.assertArrayEquals(expected, actual);
   }
}
